#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibraryS1.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibraryS1.X.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../graphicsHostLibraryCommon/clearSerial.c ../graphicsHostLibraryCommon/TFTgetTouch.c ../graphicsHostLibraryCommon/delay.c ../graphicsHostLibraryCommon/TFTGaddPoint.c ../graphicsHostLibraryCommon/putString.c ../graphicsHostLibraryCommon/sendWord.c ../graphicsHostLibraryCommon/idleSync.c ../graphicsHostLibraryCommon/TFTsend2.c ../graphicsHostLibraryCommon/putch.c ../graphicsHostLibraryCommon/TFTGcolors.c ../graphicsHostLibraryCommon/tputs.c ../graphicsHostLibraryCommon/TFTputsTT.c ../graphicsHostLibraryCommon/TFTsend4.c ../graphicsHostLibraryCommon/graphingStrings.c ../graphicsHostLibraryCommon/TFTprintDecRight.c initializeSerialPort.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/455631588/clearSerial.o ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o ${OBJECTDIR}/_ext/455631588/delay.o ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o ${OBJECTDIR}/_ext/455631588/putString.o ${OBJECTDIR}/_ext/455631588/sendWord.o ${OBJECTDIR}/_ext/455631588/idleSync.o ${OBJECTDIR}/_ext/455631588/TFTsend2.o ${OBJECTDIR}/_ext/455631588/putch.o ${OBJECTDIR}/_ext/455631588/TFTGcolors.o ${OBJECTDIR}/_ext/455631588/tputs.o ${OBJECTDIR}/_ext/455631588/TFTputsTT.o ${OBJECTDIR}/_ext/455631588/TFTsend4.o ${OBJECTDIR}/_ext/455631588/graphingStrings.o ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o ${OBJECTDIR}/initializeSerialPort.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/455631588/clearSerial.o.d ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o.d ${OBJECTDIR}/_ext/455631588/delay.o.d ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o.d ${OBJECTDIR}/_ext/455631588/putString.o.d ${OBJECTDIR}/_ext/455631588/sendWord.o.d ${OBJECTDIR}/_ext/455631588/idleSync.o.d ${OBJECTDIR}/_ext/455631588/TFTsend2.o.d ${OBJECTDIR}/_ext/455631588/putch.o.d ${OBJECTDIR}/_ext/455631588/TFTGcolors.o.d ${OBJECTDIR}/_ext/455631588/tputs.o.d ${OBJECTDIR}/_ext/455631588/TFTputsTT.o.d ${OBJECTDIR}/_ext/455631588/TFTsend4.o.d ${OBJECTDIR}/_ext/455631588/graphingStrings.o.d ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o.d ${OBJECTDIR}/initializeSerialPort.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/455631588/clearSerial.o ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o ${OBJECTDIR}/_ext/455631588/delay.o ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o ${OBJECTDIR}/_ext/455631588/putString.o ${OBJECTDIR}/_ext/455631588/sendWord.o ${OBJECTDIR}/_ext/455631588/idleSync.o ${OBJECTDIR}/_ext/455631588/TFTsend2.o ${OBJECTDIR}/_ext/455631588/putch.o ${OBJECTDIR}/_ext/455631588/TFTGcolors.o ${OBJECTDIR}/_ext/455631588/tputs.o ${OBJECTDIR}/_ext/455631588/TFTputsTT.o ${OBJECTDIR}/_ext/455631588/TFTsend4.o ${OBJECTDIR}/_ext/455631588/graphingStrings.o ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o ${OBJECTDIR}/initializeSerialPort.o

# Source Files
SOURCEFILES=../graphicsHostLibraryCommon/clearSerial.c ../graphicsHostLibraryCommon/TFTgetTouch.c ../graphicsHostLibraryCommon/delay.c ../graphicsHostLibraryCommon/TFTGaddPoint.c ../graphicsHostLibraryCommon/putString.c ../graphicsHostLibraryCommon/sendWord.c ../graphicsHostLibraryCommon/idleSync.c ../graphicsHostLibraryCommon/TFTsend2.c ../graphicsHostLibraryCommon/putch.c ../graphicsHostLibraryCommon/TFTGcolors.c ../graphicsHostLibraryCommon/tputs.c ../graphicsHostLibraryCommon/TFTputsTT.c ../graphicsHostLibraryCommon/TFTsend4.c ../graphicsHostLibraryCommon/graphingStrings.c ../graphicsHostLibraryCommon/TFTprintDecRight.c initializeSerialPort.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibraryS1.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33CH128MP505S1
MP_LINKER_FILE_OPTION=,--script=p33CH128MP505S1.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/455631588/clearSerial.o: ../graphicsHostLibraryCommon/clearSerial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/clearSerial.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/clearSerial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/clearSerial.c  -o ${OBJECTDIR}/_ext/455631588/clearSerial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/clearSerial.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/clearSerial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTgetTouch.o: ../graphicsHostLibraryCommon/TFTgetTouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTgetTouch.c  -o ${OBJECTDIR}/_ext/455631588/TFTgetTouch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTgetTouch.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTgetTouch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/delay.o: ../graphicsHostLibraryCommon/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/delay.c  -o ${OBJECTDIR}/_ext/455631588/delay.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/delay.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/delay.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o: ../graphicsHostLibraryCommon/TFTGaddPoint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTGaddPoint.c  -o ${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTGaddPoint.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/putString.o: ../graphicsHostLibraryCommon/putString.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/putString.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/putString.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/putString.c  -o ${OBJECTDIR}/_ext/455631588/putString.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/putString.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/putString.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/sendWord.o: ../graphicsHostLibraryCommon/sendWord.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/sendWord.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/sendWord.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/sendWord.c  -o ${OBJECTDIR}/_ext/455631588/sendWord.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/sendWord.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/sendWord.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/idleSync.o: ../graphicsHostLibraryCommon/idleSync.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/idleSync.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/idleSync.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/idleSync.c  -o ${OBJECTDIR}/_ext/455631588/idleSync.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/idleSync.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/idleSync.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTsend2.o: ../graphicsHostLibraryCommon/TFTsend2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTsend2.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTsend2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTsend2.c  -o ${OBJECTDIR}/_ext/455631588/TFTsend2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTsend2.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTsend2.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/putch.o: ../graphicsHostLibraryCommon/putch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/putch.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/putch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/putch.c  -o ${OBJECTDIR}/_ext/455631588/putch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/putch.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/putch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTGcolors.o: ../graphicsHostLibraryCommon/TFTGcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTGcolors.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTGcolors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTGcolors.c  -o ${OBJECTDIR}/_ext/455631588/TFTGcolors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTGcolors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTGcolors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/tputs.o: ../graphicsHostLibraryCommon/tputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/tputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/tputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/tputs.c  -o ${OBJECTDIR}/_ext/455631588/tputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/tputs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/tputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTputsTT.o: ../graphicsHostLibraryCommon/TFTputsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTputsTT.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTputsTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTputsTT.c  -o ${OBJECTDIR}/_ext/455631588/TFTputsTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTputsTT.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTputsTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTsend4.o: ../graphicsHostLibraryCommon/TFTsend4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTsend4.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTsend4.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTsend4.c  -o ${OBJECTDIR}/_ext/455631588/TFTsend4.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTsend4.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/TFTsend4.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/graphingStrings.o: ../graphicsHostLibraryCommon/graphingStrings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/graphingStrings.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/graphingStrings.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/graphingStrings.c  -o ${OBJECTDIR}/_ext/455631588/graphingStrings.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/graphingStrings.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/455631588/graphingStrings.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o: ../graphicsHostLibraryCommon/TFTprintDecRight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/455631588" 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o.d 
	@${RM} ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../graphicsHostLibraryCommon/TFTprintDecRight.c  -o ${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/455631588/TFTprintDecRight.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_I