/*  \file  graphicsHostLocal.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 29, 2015, 9:17 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHICSHOSTLOCAL_H
#define	GRAPHICSHOSTLOCAL_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! Initialize TFT */
#define TFTINIT             0x01
/*! Set color RGB */
#define TFTSETCOLOR         0x02
/*! Set Background Color RGB */
#define TFTSETBACKCOLOR     0x03
/*! Set color 16-bit */
#define TFTSETCOLORX        0x04
/*! Set background color 16-bit */
#define TFTSETBACKCOLORX    0x05
/*! Clear the screen */
#define TFTCLEAR            0x06
/*! Print teletype mode */
#define TFTPRINTTT          0x07
/*! Draw a rectangle */
#define TFTRECT             0x08
/*! Clear a rectangle */
#define TFTCLEARRECT        0x09
/*! Draw a pixel */
#define TFTPIXEL            0x0A
/*! Draw a line */
#define TFTLINE             0x0B
/*! Set the font */
#define TFTSETFONT          0x0C
/*! Print a character */
#define TFTPRINTCHAR        0x0D
/*! Draw a rounded rectangle */
#define TFTROUNDRECT        0x0E
/*! Draw a filled rounded rectangle */
#define TFTFILLROUNDRECT    0x0F
/*! Draw a circle */
#define TFTCIRCLE           0x10
/*! Draw a filled circle */
#define TFTFILLCIRCLE       0x11
/*! Draw a filled rectangle */
#define TFTFILLRECT         0x12
/*! Print a number with a decimal */
#define TFTPRINTDEC         0x13
/*! Print a number with a decimal right justified */
#define TFTPRINTDECRIGHT    0x14
/*! Print an integer */
#define TFTPRINTINT         0x15
/*! Fill screen starting at center */
#define TFTTRANSITIONOPEN   0x16
/*! Fill screen starting at edges */
#define TFTTRANSITIONCLOSE  0x17
/*! Wipe a new color on to the screen  */
#define TFTTRANSITIONWIPE   0x18
/*! Initialize a new graph */
#define TFTGINIT            0x19
/*! Ad a point to a simple graph */
#define TFTGADDPT           0x1a
/*! Render the graph frame and annotation */
#define TFTGEXPOSE          0x1b
/*! Set colors to be used for the graph */
#define TFTGCOLORS          0x1c
/* Set the X and Y ranges for the graph */
#define TFTGRANGE           0x1d
/*! Set the graph title */
#define TFTGTITLE           0x1e
/*! Set the X-axis label for the graph */
#define TFTGXLABEL          0x1f
/*! Set the Y-axis label for the graph */
#define TFTGYLABEL          0x20
/*! Turn off the graph cursor */
#define TFTGNOCURSOR        0x21
/*! Sop erasing previous graph trace */
#define TFTGNOERASE         0x22
/*! Show the currenc software version and compile date */
#define TFTSOFTWAREID       0x23
/*! Clear a portion of the screen */
#define TFTERASERECT        0x24
/*! Halt with error display */
#define TFTFATALERROR       0x25
/*! Initialize the touch system */
#define TFTTOUCHINIT        0x26  // 2
/*! Query touch data */
#define TFTTOUCHQUERY       0x27  // 2 r 4
/*! Update touch calibration */
#define TFTTOUCHCAL         0x28  // 17
/*! Make a button, simplest version */
#define TFTBUTMAKE1         0x29  // 10
/*! Query button state */
#define TFTBUTSTATE         0x2a  // 2 r 1
/*! Set the button state */
#define TFTBUTSETSTATE      0x2b  // 2
/*! Set the button visibility */
#define TFTBUTSETVIS        0x2c  // 2
/*! Set button active/inactive */
#define TFTBUTSETACT        0x2d  // 2
/*! Make a button, medium complexity */
#define TFTBUTMAKE2         0x2e
/*! Make a button, full control */
#define TFTBUTMAKE3         0x2f
/*! Control pin by button */
#define TFTBUTPIN           0x30
/*! Control button by pin */
#define TFTBUTBYPIN         0x31
/*! Print a string, teletype style */
#define TFTPUTSTT           0x82


void serialInitialize(int);
void putch( unsigned char );
void idleSync( void );
void putString(int, int, int, char *);
void sendWord( unsigned int );
void tputs(char * );
void TFTsend4( unsigned int, unsigned int, unsigned int, unsigned int, unsigned int );
void TFTsend2( unsigned int, unsigned int, unsigned int );
void graphingStrings(unsigned char, char *);
unsigned char getch(void);
void clearSerial(void);



#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSHOSTLOCAL_H */

