/*! \file  TFTsend2.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:39 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"

/*! TFTsend2 - */

/*!
 *
 */
void TFTsend2( unsigned int uCommand,
               unsigned int u1,
               unsigned int u2 )
{
  putch(uCommand);
  sendWord(u1);
  sendWord(u2);
}

