/*! \file  TFTsend4.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:38 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"


/*! TFTsend4 - */

/*!
 *
 */
void TFTsend4( unsigned int uCommand,
               unsigned int u1,
               unsigned int u2,
               unsigned int u3,
               unsigned int u4 )
{
  putch(uCommand);
  sendWord(u1);
  sendWord(u2);
  sendWord(u3);
  sendWord(u4);
}
