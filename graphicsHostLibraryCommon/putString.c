/*! \file  putString.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:48 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"


/*! putString() - Display a string on the terminal */

/*!
 *
 */
void putString( int x, int y, int delta, char *p )
{
  int xpos;
  xpos = x;

  while(*p)
    {
      putch(TFTPRINTCHAR);
      putch(*p);
      sendWord(xpos);
      sendWord(y);
      xpos+=delta;
      p++;
    }
}
