/*! \file  graphicsCH.h
 *
 *  \brief Constants and function prototypes for graphicsCH
 *
 *  \author jjmcd
 *  \date August 2, 2018, 2:52 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHICSCH_H
#define	GRAPHICSCH_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define SNORE_COUNT 1000000
#define LONG_SNORE 10000000

  void initOscillator( void );
  void snore( long );
  void toggleLEDs( int );


#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSCH_H */

