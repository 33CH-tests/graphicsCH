/*! \file  graphicsCH.c
 *
 *  \brief Test serial graphics
 *
 *  \author jjmcd
 *  \date August 2, 2018, 2:49 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <libpic30.h>
#include "graphicsCH.h"
#include "graphicsCHS1.h"


/*! graphicsCH - */

/*!
 *
 */
int main(void)
{

  /* Disable Watch Dog Timer */
  RCONbits.SWDTEN = 0;

  _TRISC8 = 0;  /* Blue LED */
  _TRISC9 = 0;  /* Pink LED */

  snore(LONG_SNORE);
  toggleLEDs(3);

  /* Program and start the slave */
  _program_slave(1, 0, graphicsCHS1);
  _start_slave();

  /* Initialize the oscillator */
  initOscillator();

  while(1)
    {

    }
}
