/*! \file  graphicsCHS1.c
 *
 *  \brief This file contains the mainline for graphicsCHS1
 *
 *
 *  \author jjmcd
 *  \date August 2, 2018, 2:54 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include "graphicsCHS1.h"
//#include "../include/graphicsHostLocal.h"
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

void rightArrow(int x, int y, int scale)
{
#define scaledline(x1,y1,x2,y2) { TFTline(x+scale*x1,y+scale*y1,x+scale*x2,y+scale*y2); }
  scaledline(0, 2, 5, 2);
  scaledline(5, 2, 5, 0);
  scaledline(5, 0, 8, 3);
  scaledline(8, 3, 5, 6);
  scaledline(5, 6, 5, 4);
  scaledline(5, 4, 0, 4);
  scaledline(0, 4, 0, 2);
  snore(1);
}

/*! main - Mainline for graphicsCHS1 */

/*!
 *
 */
int main(void)
{
  int x, y, colorset;
  char szWork[32];

  initOscillator();
  _TRISB5 = 0;
  initializeSerialPort();
  _LATB5 = 1;

  snore(SNORE_COUNT);
  TFTinit(TFTLANDSCAPE);
  snore(SNORE_COUNT);
  TFTsetBackColorX(DARKGREEN);
  TFTclear();
  snore(SNORE_COUNT);
  TFTsetColorX(LIMEGREEN);
  _LATB5 = 0;
  snore(SNORE_COUNT);
  TFTsetFont(FONTDJS);
  TFTputsTT("\r\n*** TEST ***\r\n");

  x = colorset = 0;
  y = 14;

  while (1)
    {
      LED_ORANGE ^= 1;
      //      TFTputsTT("    another\r\n");

      TFTrect(x, y, x + 10, y + 10);
      x += 17;
      if (x > 310)
        x = 0;
      y += 13;
      if (y > 234)
        {
          y = 14;
          switch (colorset)
            {
              case 0:
                TFTsetBackColorX(MIDNIGHTBLUE);
                TFTsetColorX(LIMEGREEN);
                break;
              case 1:
                TFTsetBackColorX(WHITE);
                TFTsetColorX(BLUE);
                break;
              case 2:
                TFTsetBackColorX(INDIANRED);
                TFTsetColorX(NAVAJOWHITE);
                break;
              default:
                TFTsetBackColorX(BLACK);
                TFTsetColorX(TURQUOISE);
                break;
            }
          LED_ORANGE = 0;
          snore(SNORE_COUNT);
          TFTclear();
          sprintf(szWork, "colorset %d\r\n", colorset);
          TFTputsTT(szWork);
          rightArrow(10, 20, 1);
          snore(SNORE_COUNT);
          rightArrow(50, 50, 4);
          snore(SNORE_COUNT);
          rightArrow(128, 98, 8);
          snore(SNORE_COUNT);
          rightArrow(191, 143, 16);
          snore(SNORE_COUNT);

          rightArrow(132, 101, 7);
          rightArrow(136, 104, 6);
          rightArrow(140, 107, 5);
          rightArrow(144, 110, 4);
          rightArrow(148, 113, 3);
          rightArrow(152, 116, 2);
          rightArrow(156, 119, 1);
          
          snore(LONG_SNORE);
          colorset++;
          if (colorset > 3)
            colorset = 0;
        }
      snore(SNORE_COUNT);
    }

  return 0;
}
