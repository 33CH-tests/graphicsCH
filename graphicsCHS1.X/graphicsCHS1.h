/*! \file  graphicsCHS1.h
 *
 *  \brief Constants and function prototypes for graphicsCHS1
 *
 *  \author jjmcd
 *  \date August 2, 2018, 2:55 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHICSCHS1_H
#define	GRAPHICSCHS1_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define LED_ORANGE _LATB5
#define SNORE_COUNT   500000
#define LONG_SNORE  50000000

void initOscillator(void);
void snore( long );
void initializeSerialPort();



#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSCHS1_H */

